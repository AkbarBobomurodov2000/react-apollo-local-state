import React from "react";
import Header from "./containers/Header";
import Main from ".//containers/MainSection";
function App() {
  return (
    <div className="App">
      <Header />
      <Main />
    </div>
  );
}

export default App;
