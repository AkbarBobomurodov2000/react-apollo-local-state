import React from "react";
import Header from "../components/Header";
import { todomutations } from "../operations/mutations";

export default function () {
  return <Header addTodo={todomutations.addTodo} />;
}
