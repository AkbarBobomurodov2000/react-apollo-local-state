import { from } from "@apollo/client";
import React from "react";
import { visibilityFilterVar, todosVar } from "../cashe";
import TodoList from "../components/TodoList";

import {
  VisibilityFilter,
  VisibilityFilters,
} from "../models/VisibilityFilter";
import { Todos } from "../models/Todos";

import { todomutations } from "../operations/mutations";

function filterTodosByVisibility(
  visibilityFilter: VisibilityFilter,
  todos: Todos
) {
  switch (visibilityFilter.id) {
    case VisibilityFilters.SHOW_ALL.id:
      return todos;
    case VisibilityFilters.SHOW_COMPLETED.id:
      return todos.filter((t) => t.completed);
    case VisibilityFilters.SHOW_ACTIVE.id:
      return todos.filter((t) => !t.completed);
    default:
      throw new Error("Unknown filter: " + visibilityFilter);
  }
}

function VisibleTodoList() {
  const { completeTodo, deleteTodo, editTodo } = todomutations;

  const todos = todosVar();
  const filteredTodos = filterTodosByVisibility(visibilityFilterVar(), todos);

  return (
    <TodoList
      filterTodos={filteredTodos}
      actions={{ completeTodo, deleteTodo, editTodo }}
    />
  );
}

export default VisibleTodoList;
