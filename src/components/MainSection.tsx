import React from "react";
import VisibleTodoList from "../containers/VisibleTodoList";
import { VisibilityFilter } from "../models/VisibilityFilter";
interface IMainsectionProps {
  activeVisibilityFilter: VisibilityFilter;
  todosCount: number;
  completedCount: number;
  actions: any;
}

const MainSection: React.FC<IMainsectionProps> = ({
  activeVisibilityFilter,
  todosCount,
  completedCount,
  actions,
}) => {
  return (
    <section className="main">
      {!!todosCount && (
        <span>
          <input
            type="checkbox"
            className="toggle-all"
            readOnly
            checked={completedCount === todosCount}
          />
        </span>
      )}
      <VisibleTodoList />
    </section>
  );
};

export default MainSection;
