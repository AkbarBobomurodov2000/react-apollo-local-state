import { strict } from "assert";
import React from "react";
import TodoTextInput from "./TodoTextInput";

interface IHeaderProps {
  addTodo?: (text: string) => void;
}

const Header: React.FC<IHeaderProps> = ({ addTodo }) => {
  return (
    <header className="header">
      <h1>Todos</h1>
      <TodoTextInput
        newTodo
        onSave={(text: string) => {
          console.log("text", text);
          if (text.length !== 0) {
            if (typeof addTodo === "function") {
              addTodo(text);
            }
          }
        }}
        placeholder="What needs to be done?"
      />
    </header>
  );
};

export default Header;
