import React, { useState } from "react";
import classnames from "classnames";

import { Todo } from "../models/Todos";
import { todomutations } from "../operations/mutations";
import TodoTextInput from "./TodoTextInput";
export interface ITodoItem {
  todo: Todo;
  editTodo: (id: number, text: string) => void;
  deleteTodo: (id: number) => void;
  completeTodo: (id: number) => void;
}

const TodoItem: React.FC<ITodoItem> = (props) => {
  const [editing, setEditing] = useState<boolean>(false);

  const handleDoubleClick = () => setEditing(true);

  const handleSave = (id: number, text: string) => {
    if (text.length === 0) {
      props.deleteTodo(id);
    } else {
      props.editTodo(id, text);
    }
  };

  let element;

  if (editing) {
    element = (
      <TodoTextInput
        text={props.todo.text}
        editing={editing}
        onSave={(text: string) => handleSave(props.todo.id, text)}
      />
    );
  } else {
    element = (
      <div className="view">
        <input
          type="text"
          className="toggle"
          checked={props.todo.completed}
          onChange={() => props.completeTodo(props.todo.id)}
        />
        <label onDoubleClick={handleDoubleClick}>{props.todo.text}</label>
        <button
          className="destroy"
          onClick={() => props.deleteTodo(props.todo.id)}
        />
      </div>
    );
  }

  return (
    <li
      className={classnames({
        completed: props.todo.completed,
        editing: editing,
      })}
    >
      {element}
    </li>
  );
};

export default TodoItem;
