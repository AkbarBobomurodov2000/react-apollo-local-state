import React, { useEffect, useState } from "react";
import classnames from "classnames";

interface ITodoTextInput {
  onSave: (text: string) => void;
  text?: string;
  placeholder?: string;
  editing?: boolean;
  newTodo?: boolean;
}

const TodoTextInput: React.FC<ITodoTextInput> = (props) => {
  const [text, setText] = useState<string | undefined>("");

  useEffect(() => {
    setText(props.text);
  }, [props.text]);

  const handleSubmit = (e: any) => {
    const text = e.target.value.trim();

    console.log(e.which);

    if (e.which === 13) {
      props.onSave(text);
      if (props.newTodo) {
        setText("");
      }
    }
  };

  const handleChange = (e: any) => {
    setText(e.target.value);
    props.onSave(e.target.value);
  };

  const handleBlur = (e: any) => {
    if (!props.newTodo) {
      props.onSave(e.target.value);
    }
  };

  return (
    <input
      className={classnames({ edit: props.editing, "new-todo": props.newTodo })}
      type="text"
      placeholder={props.placeholder}
      autoFocus={true}
      value={text}
      onBlur={handleBlur}
      onChange={handleChange}
      onSubmit={handleSubmit}
    />
  );
};

export default TodoTextInput;
