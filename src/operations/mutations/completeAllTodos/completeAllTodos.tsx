import { Todo, Todos } from "../../../models/Todos";
import { ReactiveVar } from "@apollo/client";

export default function completeAll(todosVar: ReactiveVar<Todos>) {
  return () => {
    const allTodosCompleted = todosVar().map((todo: Todo) => ({
      ...todo,
      completed: true,
    }));

    todosVar(allTodosCompleted);
  };
}
