import createAddTodo from "./addTodo/addTodo";
import createClearCompletedTodos from "./clearCompletedTodos/clearCompletedTodos";
import createCompleteAllTodos from "./completeAllTodos/completeAllTodos";
import createSetVisibilityFilter from "./setVisibilityFilter/setVisibilityFilter";
import createCompleteTodo from "./completeTodo/completeTodo";
import createDeleteTodo from "./deleteTodo/deleteTodo";
import createEditTodo from "./editTodo/editTodo";
import { todosVar, visibilityFilterVar } from "../../cashe";
export const todomutations = {
  addTodo: createAddTodo(todosVar),
  clearCompletedTodos: createClearCompletedTodos(todosVar),
  completeTodo: createCompleteTodo(todosVar),
  completeAllTodos: createCompleteAllTodos(todosVar),
  deleteTodo: createDeleteTodo(todosVar),
  editTodo: createEditTodo(todosVar),
  setVisibilityFilter: createSetVisibilityFilter(visibilityFilterVar),
};
